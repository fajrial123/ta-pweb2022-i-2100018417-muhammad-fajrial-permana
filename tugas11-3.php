<!DOCTYPE html>
<html>
<head>
    <title>Form input type checkbox</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Pilih Genre Film Kesukaan Anda</h2>
        <input type="checkbox" name="film1" value="Action"> Action <br/>
        <input type="checkbox" name="film2" value="Horror"> Horror <br/>
        <input type="checkbox" name="film3" value="Drama"> Drama <br/>
        <input type="submit" name="Input" value="Pilih">
    </form>
</body>
</html>

<?php
    echo "Pilih Genre Film Kesukaan Anda : <br>";
    if (isset($_POST['film1'])){
        echo "- " . $_POST['film1'] . "<br>";
    }
    if (isset($_POST['film2'])){
        echo "- " . $_POST['film2'] . "<br>";
    }
    if (isset($_POST['film3'])){
        echo "- " . $_POST['film3'] . "<br>";
    }
?>

