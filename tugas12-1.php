<html>
<head>
<title>Array for dan foreach</title>
<style type="text/css">
    body{
        background:#42B9D0; 
    }
</style>
</head>
<body>
</body>
</html>

<?php
$arrWarna = array("Red", "green", "yellow");
echo "Menampilkan isi array dengan FOR : <br>";
for ($i = 0; $i < count($arrWarna); $i++){
    echo "Warna kesukaanku <font color = $arrWarna[$i]>" .$arrWarna[$i]. "</font><br>";
}

echo "<br> Menampilkan isi array dengan FOREACH : <br>";
foreach ($arrWarna as $warna) {
    echo " Warna kesukaanku <font color = $warna>". $warna ."</font><br>";
}
?>
