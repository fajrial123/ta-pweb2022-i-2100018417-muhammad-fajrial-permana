<html>
<head>
<title> Array dengan sort() dan rsort() </title>
<style type="text/css">
    body{
        background:#5A7DAF; 
    }
</style>
</head>
<body>
</body>
</html>

<?php
$arrNilai = array("Fajri"=>"3.89", "Dinda"=>"3.78", "Nisa"=>"3.60", "Somad"=>"3.77", "Winda"=>"3.45", "Nando"=>"3.66", "Nemo"=>"3.70", "Doni"=>"3.65");
echo "<b> Array sebelum diurutkan  : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b> Array setelah diurutkan dengan sort : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b> Array setelah diurutkan dengan rsort : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>
