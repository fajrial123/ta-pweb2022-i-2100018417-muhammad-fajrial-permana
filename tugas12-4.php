<html>
<head>
<title> Array dengan asort() dan arsort() </title>
<style type="text/css">
    body{
        background:#5C7DFA; 
    }
</style>
</head>
<body>
</body>
</html>

<?php
$arrNilai = array("Fajri"=>"3.89", "Dinda"=>"3.78", "Nisa"=>"3.60", "Somad"=>"3.77", "Winda"=>"3.45", "Nando"=>"3.66", "Nemo"=>"3.70", "Doni"=>"3.65");
echo "<b> Array sebelum diurutkan  : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

asort($arrNilai);
reset($arrNilai);
echo "<b> Array setelah diurutkan dengan asort : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

arsort($arrNilai);
reset($arrNilai);
echo "<b> Array setelah diurutkan dengan arsort : <b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";
?>
